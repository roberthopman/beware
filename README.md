### Beware

Beware ("Be aware") is a simple app with users, roles, todos, nofitications, etc.

### Requirements

- Postgresql
- Ruby 2.6.5
- Rails 6.0.2
- Redis
- Webpacker
- Bootstrap 5.0.0-beta3

### Setup in development

```
bundle install
npm install
rails db:setup
rails setup:data
```

### Quickstart in development


`foreman start`

### Start in development

```
redis-server
bundle exec sidekiq -q default -q mailers
./bin/webpack-dev-server
rails server
mailcatcher
```

### Running tests

`redis-server`
`rails test`

### delivery methods

- Telegram (https://t.me/be_aware_bot) sends a notification to a chat_id. Delivery of a notification first requires to manually start the conversation with the bot. Chat_id must be kept private and can be taken from starting a conversation with https://telegram.me/JsonDumpBot. 
- Email

# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
* System dependencies
* Configuration
* Database creation
* Database initialization
* How to run the test suite
* Services (job queues, cache servers, search engines, etc.)
* Deployment instructions
* ...

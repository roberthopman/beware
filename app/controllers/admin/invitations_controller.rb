class Admin::InvitationsController < Devise::InvitationsController
  def create
    if invited_user_params[:name].present?
      @user = User.invite!(invited_user_params) { |invitable|  
        invitable.user_accounts.new(account: current_tenant)
        add_user_roles(invitable)
      }
    else
     @user = User.create(email: invited_user_params[:email])
    end
    
    if @user.persisted?
      redirect_to admin_users_path, notice: "#{invited_user_params[:email]} has been invited."
    else
      render "admin/invitations/new", locals: {resource: @user, resource_name: resource_name}
    end
  end

  private

  def add_user_roles(invitable)
    if role_params[:role_ids].reject(&:blank?).present?
      submitted_user_role_ids = role_params[:role_ids].reject(&:blank?).map(&:to_i)
      submitted_user_role_ids.each do |id|
        invitable.roles << Role.find_by(role_name: id)
      end
    end
  end

  def role_params
    params.require(:user).permit(role_ids: [])
  end

  def invited_user_params
    params.require(:user).permit(:name, :email, :invitation_token)
  end
end

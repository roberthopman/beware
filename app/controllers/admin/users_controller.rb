class Admin::UsersController < ApplicationController
  before_action :find_user, only: [:show, :edit, :update, :archive]

  def index
    @user_accounts = current_tenant.user_accounts.joins(:user).order("users.last_name")
  end

  def show
  end

  def edit
  end

  def update
    new_roles(user_params[:role_ids])
    update_user_roles(@user, @new_roles)
    redirect_to admin_user_path
  end

  def archive
    current_tenant.user_accounts.find_by(user_id: @user.id).toggle!(:archived)
    redirect_to admin_users_path
  end

  private

  def new_roles(params)
    submitted_user_role_ids = params.reject(&:blank?).map(&:to_i)
    @new_roles = []
    submitted_user_role_ids.each { |id| @new_roles << Role.find_by(role_name: id) }
  end

  def update_user_roles(user, new_roles)
    Role.role_names.keys.each do |key|
      roles_include_role = user.roles.map { |r| r.role_name }.include?(key)
      new_roles_include_role = new_roles.map { |r| r.role_name }.include?(key)
      role = Role.send(key)
      if roles_include_role && !new_roles_include_role
        user_role = user.user_roles.where(role: role).first
        if !user_role.destroy
          flash.alert = "At least 1 owner"
        else
          flash.notice = "Role(s) updated"
        end
      elsif new_roles_include_role && !roles_include_role
        if user.roles << role
          flash.notice = "Role(s) updated"
        end
      end
    end
  end

  def find_user
    user_id = params[:id] || params[:user_id]
    if current_tenant.users.find_by(id: user_id).present?
      @user = current_tenant.users.find_by(id: user_id)
    else
      redirect_to admin_user_path
    end
  end

  def user_params
    params.require(:user).permit(role_ids: [])
  end
end

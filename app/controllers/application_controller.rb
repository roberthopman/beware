class ApplicationController < ActionController::Base
  include ActionView::RecordIdentifier
  include RecordHelper
  set_current_tenant_through_filter
  before_action :find_current_tenant
  before_action :find_tenants
  before_action :authenticate_user!, except: [:hello]
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :current_user_account
  before_action :set_current_tenant_user_accounts

  def hello
    render "layouts/hello.html.erb"
  end

  def select_tenant
    session[:current_tenant_id] = params[:id]
    redirect_to root_path
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, [accounts_attributes: [:name]]])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name])
    devise_parameter_sanitizer.permit(:invite, keys: [:name, role_ids: []])
  end

  def after_sign_in_path_for(resource)
    root_path
  end

  def find_current_tenant
    if user_signed_in?
      if session[:current_tenant_id].present?
        account = Account.find_by(id: session[:current_tenant_id])
        if account.present?
          set_current_tenant(account)
        else
          session[:current_tenant_id] = ""
          redirect_to work_items_path
        end
      else
        set_current_tenant(current_user.accounts.first)
      end
    end
  end

  def find_tenants
    if user_signed_in?
      @accounts = current_user.accounts.sort
    end
  end

  def current_user_account
    if current_tenant.present?
      if current_tenant.user_accounts.present?
        session[:current_tenant_id] = current_tenant.id if session[:current_tenant_id].nil?
        current_user_account = current_tenant.user_accounts.find_by(user: current_user)
        if !current_user_account.present?
          session[:current_tenant_id] = ""
          redirect_to work_items_path
        else
          current_user_account
        end
      else
        session[:current_tenant_id] = ""
        redirect_to work_items_path
      end
    end
  end

  def set_current_tenant_user_accounts
    if current_tenant
      @current_tenant_user_accounts = current_tenant.user_accounts.map{|obj| [obj.user.name, obj.id] }
    end
  end
end

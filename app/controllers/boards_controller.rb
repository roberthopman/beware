class BoardsController < ApplicationController
  before_action :set_board, only: [:show, :edit, :update, :destroy]
  before_action :get_templates, only: [:new, :edit]
  layout "board", only: [:show]

  # GET /boards
  def index
    @boards = Board.all
  end

  # GET /boards/1
  def show
  end

  # GET /boards/new
  def new
    @board = Board.new
  end

  # GET /boards/1/edit
  def edit
  end

  # POST /boards
  def create
    @board = Board.new(board_params)

    if @board.save
      redirect_to @board, notice: 'Board was successfully created.'
    else
      get_templates
      respond_to do |format|
        format.turbo_stream {
          render turbo_stream: turbo_stream.replace(dom_id_for_records(@board), partial: "form", locals: { board: @board })
        }
      end
    end
  end

  # PATCH/PUT /boards/1
  def update
    if @board.update(board_params)
      redirect_to @board, notice: 'Board was successfully updated.'
    else
      get_templates
      respond_to do |format|
        format.turbo_stream {
          render turbo_stream: turbo_stream.replace(dom_id_for_records(@board), partial: "form", locals: { board: @board })
        }
      end
    end
  end

  # DELETE /boards/1
  def destroy
    @board.destroy
    redirect_to boards_url, notice: 'Board was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_board
      @board = Board.find(params[:id])
    end

    def get_templates
      @templates = Template.all.map{|t| [t.title, t.id]}
    end

    # Only allow a trusted parameter "white list" through.
    def board_params
      params.require(:board).permit(:title, :description, :template_id, :account_id)
    end
end

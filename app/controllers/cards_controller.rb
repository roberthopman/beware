class CardsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_card

  def show
  end

  def edit
  end

  def update
    if @card.update(card_params)
      redirect_to @card
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @card.destroy
    respond_to do |format|
      format.turbo_stream {}
      format.html { redirect_to @card.list.board }
    end
  end

  def move
    @card.list_id = params[:list_id].to_i
    @card.position = params[:position].to_i
    @card.save

    head :ok
  end

  private
    def set_card
      @card = Card.find(params[:id])
    end

    def card_params
      params.require(:card).permit(:title)
    end
end

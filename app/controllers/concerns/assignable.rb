module Assignable
  extend ActiveSupport::Concern

  included do
    helper_method :add_creator
    helper_method :add_allocation
  end

  def add_creator(user_account, object)
    Action.create(
      actor_id: user_account.id, 
      action_type: :created, 
      action_object_type: object.class.name, 
      action_object_id: object.id)
  end

  def add_allocation(actor_id, object, recipient_id)
    Action.create(
      actor_id: actor_id,
      recipient_id: recipient_id,
      action_type: :allocated,
      action_object_type: object.class.name,
      action_object_id: object.id)
  end
end

class FieldValuesController < ApplicationController
  def create
    form_id = params[:form_id].to_i
    field_id = params[:field_id].to_i
    @field_value = FieldValue.new(value: field_value_params[:value], 
      form_id: form_id, field_id: field_id)
    if @field_value.save
      flash[:notice] = 'Answer was successfully created.'
      redirect_to form_path(@field_value.form)
    else
      flash[:alert] = 'Answer was empty.'
      redirect_to form_path(@field_value.form)
    end
  end

  private

  def field_value_params
    params.require(:field_value).permit(:value)
  end
end

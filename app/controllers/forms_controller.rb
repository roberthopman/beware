class FormsController < ApplicationController
  include Assignable
  before_action :set_form, only: [:show, :pause, :edit, :update]
  before_action :set_forms, only: [:index]  

  def index
  end

  def new
    @time_of_day = Time.now.strftime("%H:%m")
    @form = Form.new
  end

  def show
    @field = @form.fields.first
    @field_value = @form.fields.first.field_values.build
    @grouped_by_day_field_values = @field.field_values.reverse.group_by { |field_value|
      field_value.created_at.strftime("%d %B %Y")
    }
  end

  def create
    @form = Form.new(description: form_params[:description])
    @recurrence = form_params[:schedule_recurrence]
    @time_of_day = Time.parse(form_params[:schedule_time_of_day])
    @user_account_ids = form_params[:allocation_ids].reject(&:blank?).map(&:to_i)

    if @form.valid? && @recurrence != 'null' && !@user_account_ids.empty?
      @form.save   
      @form.fields.create(name: 'question_answer', field_type: Field.field_types[:string])
      create_schedule(form_params)
      allocate_users(current_user_account, @form, @user_account_ids)
      add_creator(current_user_account, @form)

      respond_to do |format|
          format.html  { redirect_to(@form,
                        :notice => 'Question was successfully created.') }
      end
    else      
      @recurrence == 'null' ? @form.errors.add(:schedule_recurrence, "can't be blank") : nil
      @user_account_ids.empty? ? @form.errors.add(:allocations, "can't be blank") : @user_account_ids
      @current_tenant_user_accounts = current_tenant.user_accounts.map{|obj| [obj.user.name, obj.id] }
      @time_of_day = form_params[:schedule_time_of_day]

      render :new
    end
  end

  def edit
  end

  def update
    @user_account_ids = form_params[:allocation_ids].reject(&:blank?).map(&:to_i)
    if form_params[:description].present? && !@user_account_ids.empty?
      @form.update(description: form_params[:description])
      allocate_users(current_user_account, @form, @user_account_ids)

      respond_to do |format|
        format.html  { redirect_to(@form,
                      :notice => 'Question was successfully updated.') }
      end
    else
      form_params[:description].empty? ? @form.errors.add(:description, "can't be blank") : ""
      @user_account_ids.empty? ? @form.errors.add(:allocations, "can't be blank") : @user_account_ids
      @current_tenant_user_accounts = current_tenant.user_accounts.map{|obj| [obj.user.name, obj.id] }
      render :edit
    end
  end

  def allocate_users(actor, object, allocated_ids)
    deallocate_users(actor, object, allocated_ids)
  
    allocated_ids.each do |id|
      if !id.in? object.allocations.map(&:recipient_id) 
        object.allocations.create(
          actor_id: actor.id, 
          recipient_id: id, 
          action_object_type: object.class.name,
          action_object_id: object.id
        )
      end
    end
  end

  def deallocate_users(actor, object, allocated_ids)
    if object.allocations.present?
      object.allocations.each do |allocation|
        if !allocation.recipient_id.in? allocated_ids
          allocation.update(
            actor_id: actor.id,
            action_type: :deallocated)
        end
      end
    end
  end

  def create_schedule(form_params)
    recurrence = form_params[:schedule_recurrence]
    time_of_day = Time.parse(form_params[:schedule_time_of_day])
    @form.events.create(start_time: time_of_day, recurring: recurrence)
  end

  def pause
    @form.toggle!(:paused)
    redirect_to @form
  end

  private

  def set_form
    id = params[:id] || params[:form_id]
    @form = Form.find(id)
  end

  def set_forms
    @questions = Form.all
    @allocated_questions = current_user_account.allocated_forms
  end

  def form_params
    params.require(:form).permit(:description, :schedule_recurrence, 
      :schedule_time_of_day, allocation_ids: [])
  end
end

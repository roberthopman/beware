class Lists::CardsController < ApplicationController
  include Assignable
  before_action :authenticate_user!
  before_action :set_list_and_board

  def new
    card = Card.new
    render turbo_stream: turbo_stream.replace(dom_id(@list), partial: "cards/form", locals: { card: card, list: @list, board: @board })
  end

  def create
    @card = @list.cards.new(card_params)

    respond_to do |format|
      if @card.save
        add_creator(current_user_account, @card)
        card = Card.new
        format.turbo_stream {
          render turbo_stream: turbo_stream.replace(dom_id_for_records(@list, card), partial: "cards/new", locals: { card: card, list: @list })
        }
        format.html { redirect_to @board }
      else
        format.turbo_stream {
          render turbo_stream: turbo_stream.replace(dom_id_for_records(@list, @card), partial: "cards/form", locals: { card: @card, list: @list })
        }
        format.html { redirect_to @board }
      end
    end
  end

  private

  def set_list_and_board
    @list = List.find(params[:list_id])
    @board = @list.board
  end

  def card_params
    params.require(:card).permit(:title)
  end
end

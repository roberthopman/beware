class ListsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_list_and_board

  def cancel  
    card = Card.new
    render turbo_stream: turbo_stream.replace(dom_id_for_records(@list, card), partial: "cards/new", locals: { list: @list })
  end
  
  private
    def set_list_and_board
      @list = List.find(params[:id])
      @board = @list.board
    end

    def list_params
      params.require(:list).permit(:id)
    end
end

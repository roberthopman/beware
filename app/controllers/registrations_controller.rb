class RegistrationsController < Devise::RegistrationsController
  def new
    build_resource({})
    resource.accounts << Account.new
    respond_with resource
  end
      
  def create
    build_resource(sign_up_params)
    
    create_account_and_role_and_resource

    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication?
        set_flash_message! :notice, :signed_up
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      respond_with resource
    end
  end  

  protected

  def create_account_and_role_and_resource
    account = resource.accounts.first
    if account.name.present? && account.valid?
      account.save      
      set_current_tenant(account)
      resource.roles << Role.find_by(role_name: :owner)
      resource.save
    else
      resource.save
      resource.errors.delete(:roles)
    end
  end

  def after_sign_up_path_for(resource)
    work_items_path
  end
end

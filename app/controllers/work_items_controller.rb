class WorkItemsController < ApplicationController
  include Assignable
  before_action :find_work_items
  before_action :set_time_amount_and_scale, only: [:new, :create]
  helper WorkItemsHelper

  def index
  end

  def new
    @work_item = WorkItem.new(title: "foo")
  end

  def create
    @work_item = WorkItem.new(work_item_params)
    @work_item.set_deadline(work_item_params)
    @recipient_id = work_item_params[:responsible_person_id] 
    @amount_selected = work_item_params[:time_amount]
    
    if @work_item.valid?
      @work_item.save
      add_creator(current_user_account, @work_item)
      add_allocation(current_user_account.id, @work_item, @recipient_id)
      # Notification.schedule_notifications(@work_item)
      redirect_to work_items_path
    else
      render new_work_item_path
    end
  end

  def update
    work_item = WorkItem.find(params[:id])
    if work_item.update(finished_at: Time.current)
      # Notification.unschedule_notifications(work_item)
      redirect_to work_items_path
    end
  end

  def cancel
    work_item = WorkItem.find(params[:work_item_id])
    if work_item.update(canceled_at: Time.current)
      # Notification.unschedule_notifications(work_item)
      redirect_to work_items_path
    end
  end

  private

  def find_work_items
    @work_items = current_user_account.created_work_items.enabled
    @work_items_finished = WorkItem.finished
    @work_items_overdue = WorkItem.overdue
  rescue ActiveRecord::RecordNotFound
    render status: 404
  end

  def set_time_amount_and_scale
    @amounts = WorkItem::TIME_AMOUNTS
    @amount_selected ||= @amounts.fifth
    @scales = WorkItem::TIME_SCALES
    @scale_selected ||= @scales.first
    @recipient_id = current_user_account.id
  end

  def work_item_params
    params.require(:work_item).permit(:title, :time_amount, :time_scale, :responsible_person_id)
  end
end

module WorkItemsHelper
  TWO_DAYS_IN_SECONDS = 60 * 60 * 48
  ABOUT_ONE_MONTH_LOWER_BOUND_FROM_TIME_AGO_IN_WORDS = 29 * 60 * 60 * 24 + 23 * 3600 + 59 * 60 + 30

  def format_initial_time(deadline)
    seconds = (deadline - Time.current).to_i
    if seconds > ABOUT_ONE_MONTH_LOWER_BOUND_FROM_TIME_AGO_IN_WORDS
      days = (seconds / (3600 * 24))
      days.to_s + " days"
    elsif seconds > TWO_DAYS_IN_SECONDS
      time_ago_in_words(deadline)
    else
      convert_seconds_into_hours(seconds)
    end
  end

  def convert_seconds_into_hours(seconds)
    hours = seconds / (60 * 60)
    remaining_seconds = seconds - (hours * 60 * 60)
    minutes = remaining_seconds / 60
    seconds = remaining_seconds - (minutes * 60)
    add_zeros(hours, minutes, seconds)
    "#{@hours}h #{@minutes}m #{@seconds}s"
  end

  def add_zeros(hours, minutes, seconds)
    @hours = hours < 10 ? "0" + hours.to_s : hours.to_s
    @minutes = minutes < 10 ? "0" + minutes.to_s : minutes.to_s
    @seconds = seconds < 10 ? "0" + seconds.to_s : seconds.to_s
  end
end

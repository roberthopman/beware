import { Controller } from "stimulus"
import Sortable from "sortablejs"
import Rails from "rails-ujs"

export default class extends Controller {
  connect() {
    this.sortable = Sortable.create(this.element, {
      group: 'cards',
      onEnd: this.end.bind(this)
    })
  }

  end(event) {
    let id = event.item.id 
    let list_id = event.to.dataset.listId

    let data = new FormData()
    data.append("position", event.newIndex + 1)
    data.append("list_id", list_id)

    if (!(event.from.dataset.listId == event.to.dataset.listId && event.newIndex == event.oldIndex)){
      Rails.ajax({
        url: this.data.get("url").replace(":id", id),
        type: 'PATCH',
        data: data
      })
    }
    
  }
}

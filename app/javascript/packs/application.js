import $ from 'jquery';
import 'bootstrap/dist/js/bootstrap';
import 'core-js/stable'
import 'regenerator-runtime/runtime'
import './stylesheet.scss'
import '../../../vendor/javascripts/recurring_select.js.erb'
import "@hotwired/turbo-rails"
import "../controllers"
import 'rails-ujs'

require("rails-ujs").start()

const Choices = require('choices.js')

function changeTimeScale(){
  var amount_element = document.getElementById("work_item_time_amount");
  var scale_element = document.getElementById("work_item_time_scale");
  var amount = amount_element.value;
  var scale = scale_element.value;
  var length = scale_element.options.length;
  for (var i=0; i<length; i++){
    if (amount == 1 && scale.slice(-1) == 's') {
      var value = scale_element.options[i].value;
      scale_element.options[i].value = value.substr(0, value.length - 1);
      scale_element.options[i].text = value.substr(0, value.length - 1);
    } else if (amount != 1 && scale.slice(-1) != 's') {
      var value = scale_element.options[i].value;
      scale_element.options[i].value = value + 's';
      scale_element.options[i].text = value + 's';
    };
  };
};

function startTimer(duration, display) {
  var timer = duration, hours, minutes, seconds, twoDays, sec_num, days;
  setInterval(function () {
      twoDays = 2*24*60*60;
      if (duration < twoDays) {
        sec_num = parseInt(timer, 10);
        hours   = Math.floor(sec_num / 3600);
        minutes = Math.floor(sec_num / 60) % 60;
        seconds = sec_num % 60;

        hours = hours < 10 ? "0" + hours : hours;
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = hours + "h " + minutes + "m " + seconds + "s";
      } 
      if (--timer < 0) {
        display.textContent = "Alert!"
      }
  }, 1000)
};

document.addEventListener('turbo:load', () => {
  var amount_element = document.getElementById("work_item_time_amount");
  if (amount_element != null) {
    amount_element.onchange = changeTimeScale;  
  };
  
  var deadlines = document.getElementsByClassName("count");
  
  for (var i=0; i<deadlines.length; i++){
    if (deadlines[i] != null) {
      var duration = deadlines[i].dataset.deadline;
      startTimer(duration, deadlines[i]);
    }
  }
});

// multi select
document.addEventListener('turbo:load', () => {
  if( $('#dropdown-choice-select').length ){
    new Choices('#dropdown-choice-select', {
      removeItemButton: true,
      shouldSort: false
    })
  }
  
  if( $('#dropdown-choice-select-has-one').length ){
    const element = new Choices('#dropdown-choice-select-has-one', {
      removeItemButton: true,
      shouldSort: false
    })

    // enable vimium
    element.passedElement.element.parentNode.parentNode.setAttribute('role', 'button');
  }
  if( $('#dropdown-choice-select-amount').length ){
    const element = new Choices('#dropdown-choice-select-amount', {
      removeItemButton: true,
      shouldSort: false
    })

    // enable vimium
    element.passedElement.element.parentNode.parentNode.setAttribute('role', 'button');
  }
  if( $('#dropdown-choice-select-scale').length ){
    const element = new Choices('#dropdown-choice-select-scale', {
      removeItemButton: true,
      shouldSort: false
    })

    // enable vimium
    element.passedElement.element.parentNode.parentNode.setAttribute('role', 'button');
  }
});

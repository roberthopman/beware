class NotifyJob < ApplicationJob
  queue_as :default
  require "telegram/bot"

  CHAT_ID = Rails.application.credentials[:alert][:telegram][:chat_id]
  TELEGRAM_TOKEN = Rails.application.credentials[:alert][:telegram][:bot][:token]

  def perform(action)
    notification = Notification.find_by(action_id: action.id)
    actor_name = UserAccount.find(action.actor_id).user.name
    recipient_account = UserAccount.find(action.recipient_id) if action.recipient_id.present?
    if action.action_object_type == "Form"    
      if action.allocated?
        payload = "#{actor_name} #{action.action_type} #{action.action_object.description} to #{recipient_account.user.name}"
      end
      if action.deallocated?
        payload = "#{actor_name} #{action.action_type} #{recipient_account.user.name} from '#{action.action_object.description}'"
      end
    elsif action.action_object_type == "WorkItem"
      minutes = (action.action_object.deadline - notification.scheduled_at) / 60
      rounded_min = minutes.ceil
      if action.allocated?
        payload = "#{actor_name} #{action.action_type} #{recipient_account.user.name} to #{action.action_object_type} #{action.action_object.title} with a time constraint of #{rounded_min} minutes"
      end
    end
    notification.update(sent_at: DateTime.now)
    NotifierMailer.notify(recipient_account.user, payload).deliver_later
    message_telegram(payload) if CHAT_ID.present? && TELEGRAM_TOKEN.present?
  end

  def message_telegram(payload)
    Telegram::Bot::Client.run(TELEGRAM_TOKEN) do |bot|
      bot.api.send_message(chat_id: CHAT_ID, text: payload)
    end
  end
end

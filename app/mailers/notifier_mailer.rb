class NotifierMailer < ApplicationMailer
  default from: "no-reply@example.com"

  def notify(user, payload)
    @payload = payload
    mail(to: user.email, subject: payload)
  end
end

class Account < ApplicationRecord
  has_many :user_accounts
  has_many :users, through: :user_accounts
  has_many :user_roles
  has_many :roles, through: :user_roles
  validates :name, presence: true
end

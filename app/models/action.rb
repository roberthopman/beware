class Action < ApplicationRecord
  acts_as_tenant(:account)
  enum action_type: {allocated: 0, created: 1, deallocated: 2}
  belongs_to :action_object, polymorphic: true
  belongs_to :actor, class_name: "UserAccount"
  belongs_to :recipient, class_name: "UserAccount", optional: true
  validates :actor_id, :action_type, :action_object_type, :action_object_id, presence: true
  after_create :notify_allocated 
  after_update :notify_deallocated

  def notify_allocated
    enqueue_notification if self.allocated?
  end

  def notify_deallocated
    enqueue_notification if self.deallocated?
  end

  def enqueue_notification
    job = NotifyJob.set(wait_until: DateTime.now).perform_later(self)
    notification = Notification.new(
      action_id: self.id,
      scheduled_at: DateTime.now,
      job_id: job.provider_job_id
    )
    notification.save
  end
end

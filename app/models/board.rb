class Board < ApplicationRecord
  acts_as_tenant(:account)
  belongs_to :template
  belongs_to :account
  validates :title, presence: true
  has_many :lists, dependent: :destroy

  before_create :create_lists

  def create_lists
    case self.template_id
    when 1
      create_problem_canvas_lists
    end
  end

  def create_problem_canvas_lists
    Template.find(1).template_lists.each do |template_list|
      self.lists << List.new(title: template_list.title, position: template_list.position)
    end
  end
end

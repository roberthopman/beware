class Card < ApplicationRecord
  include ActionView::RecordIdentifier
  
  acts_as_tenant(:account)
  belongs_to :list
  acts_as_list scope: :list
  belongs_to :account
  has_one :created_action, -> { where(action_type: 1, action_object_type: "Card") }, class_name: "Action", foreign_key: :action_object_id, dependent: :destroy
  has_one :creator, through: :created_action, source: :actor
  validates :title, presence: true

  default_scope { order(position: :asc) }

  after_create_commit do
    broadcast_append_to [list, :cards], target: "#{dom_id(list)}_cards"
  end

  after_update_commit do
    broadcast_replace_to self 
    board = self.list.board
    broadcast_replace_to(board, partial: "lists/index", locals: { board: board }, target: dom_id(board))
  end

  after_destroy_commit do
    broadcast_remove_to self
  end
end

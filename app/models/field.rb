class Field < ApplicationRecord
  acts_as_tenant(:account)
  belongs_to :form
  has_many :field_values
  enum field_type: {string: 0}
end

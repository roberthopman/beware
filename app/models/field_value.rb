class FieldValue < ApplicationRecord
  acts_as_tenant(:account)
  belongs_to :form
  belongs_to :field
  validates :value, presence: true
end

class Form < ApplicationRecord
  acts_as_tenant(:account)
  has_many :allocations, -> { where action_type: 0 }, class_name: "Action", foreign_key: :action_object_id
  has_one :creator, -> { where(action_type: 1, action_object_type: "Form") }, class_name: "Action", foreign_key: :action_object_id
  has_many :actions, foreign_key: :action_object_id
  has_many :form_events
  has_many :events, through: :form_events
  has_many :fields
  validates :description, presence: true
  attr_accessor :schedule_recurrence, :schedule_time_of_day

  def schedule
    JSON.parse(first_event.recurring)
  end

  def rule
    IceCube::Rule.from_hash(schedule).to_s.downcase
  end

  def time_of_day
    first_event.start_time.strftime("%H:%M")
  end

  def first_event
    self.events.first
  end

  def allocated_user_account_ids
    self.allocations.map { |x| UserAccount.find(x.recipient_id) }.map(&:id)
  end
end

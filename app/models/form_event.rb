class FormEvent < ApplicationRecord
  acts_as_tenant(:account)
  belongs_to :form
  belongs_to :event
end

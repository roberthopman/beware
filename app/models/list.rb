class List < ApplicationRecord
  acts_as_tenant(:account)
  belongs_to :account
  belongs_to :board
  has_many :cards, dependent: :destroy
end

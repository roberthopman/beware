class Notification < ApplicationRecord
  acts_as_tenant(:account)
  belongs_to :action, class_name: "Action", foreign_key: :action_id, optional: true
  belongs_to :work_item, class_name: "WorkItem", foreign_key: :action_object_id, optional: true
  belongs_to :recipient, class_name: "User", foreign_key: :recipient_id, optional: true

  MINUTES_TO_BE_NOTIFIED = [5, 10, 15, 30, 60, 120]

  def self.schedule_notifications(object)
    @object = object
    minutes = (@object.deadline - DateTime.current) / 60
    rounded_min = minutes.ceil
    selected_minutes = MINUTES_TO_BE_NOTIFIED.select { |num| rounded_min >= num }
    create_notifications(selected_minutes)
  end

  def self.create_notifications(selected_minutes)
    selected_minutes.each do |minutes|
      create_notification(minutes)
    end
  end

  def self.create_notification(number)
    notification = Notification.new(
      # action_id: @object.id,
      scheduled_at: @object.deadline - number.minutes
    )
    notification.save
    job = NotifyJob.set(wait_until: notification.scheduled_at).perform_later(notification)
    notification.update(job_id: job.provider_job_id)
  end

  def self.unschedule_notifications(object)
    if object.notifications.present?
      object.notifications.where(sent_at: nil).each do |n|
        delete_notification(n)
      end
    end
  end

  def self.delete_notification(object)
    Sidekiq::ScheduledSet.new.delete_by_jid(object.scheduled_at.to_f, object.job_id)
    object.delete
  end
end

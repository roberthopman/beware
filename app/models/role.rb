class Role < ApplicationRecord
  has_many :user_roles
  enum role_name: {owner: 1, admin: 2, user: 3}
end

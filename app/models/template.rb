class Template < ApplicationRecord
  acts_as_tenant(:account)
  belongs_to :account
  has_many :boards
  validates :title, presence: true
  has_many :template_lists, dependent: :destroy

  after_create :create_lists

  def create_lists
    case self.id
    when 1
      create_problem_solution_canvas_template_lists
    end
  end

  def create_problem_solution_canvas_template_lists
    self.template_lists << [
      TemplateList.new(title: "Customer Persona", position: 1),
      TemplateList.new(title: "Customer limitations", position: 2),
      TemplateList.new(title: "Available solutions", position: 3),
      TemplateList.new(title: "Problems / Pains / Jobs-to-be-done", position: 4),
      TemplateList.new(title: "Problem Root / Cause", position: 5),
      TemplateList.new(title: "Behaviour", position: 6),
      TemplateList.new(title: "Triggers to act", position: 7),
      TemplateList.new(title: "Emotions before / after", position: 8),
      TemplateList.new(title: "Your solution", position: 9),
      TemplateList.new(title: "Online channels of behaviour", position: 10),
      TemplateList.new(title: "Offline channels of behaviour", position: 11)
    ]
  end
end

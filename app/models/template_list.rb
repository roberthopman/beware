class TemplateList < ApplicationRecord
  acts_as_tenant(:account)
  belongs_to :template
  belongs_to :account
  validates :title, presence: true
end

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
    :recoverable, :rememberable, :validatable, :trackable
  has_person_name
  has_many :user_accounts
  has_many :accounts, through: :user_accounts
  has_many :user_roles
  has_many :roles, through: :user_roles
  accepts_nested_attributes_for :accounts
  validates :name, :email, presence: true
  validates :roles, presence: true
  before_create :has_roles
  
  def has_roles
    if user_roles.size == 0
      errors[:roles] << "can't be blank"
      throw(:abort)
    end
  end
end

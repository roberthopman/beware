class UserAccount < ApplicationRecord
  belongs_to :user
  belongs_to :account
  has_many :form_allocated_actions, -> { where(action_type: 0, action_object_type: "Form") }, class_name: "Action", foreign_key: :recipient_id
  has_many :allocated_forms, through: :form_allocated_actions, source: :action_object, source_type: "Form"  
  has_many :work_item_created_actions, -> { where(action_type: 1, action_object_type: "WorkItem") }, class_name: "Action", foreign_key: :actor_id
  has_many :created_work_items, through: :work_item_created_actions, source: :action_object, source_type: "WorkItem"
  has_many :card_created_actions, -> { where(action_type: 1, action_object_type: "Card") }, class_name: "Action", foreign_key: :actor_id
  has_many :created_cards, through: :card_created_actions, source: :action_object, source_type: "Card"
end

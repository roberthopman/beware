class UserRole < ApplicationRecord
  acts_as_tenant(:account)
  belongs_to :account
  belongs_to :user
  belongs_to :role
  before_destroy :check_account_owner_presence

  def check_account_owner_presence
    role_is_owner = role_id == Role.role_names[:owner]
    if role_is_owner && is_last_owner?
      throw(:abort)
    end
  end

  def is_last_owner?
    UserRole.where(role: Role.owner).count - 1 == 0
  end
end

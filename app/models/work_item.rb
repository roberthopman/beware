class WorkItem < ApplicationRecord
  acts_as_tenant(:account)
  has_one :allocated_action, -> { where(action_type: 0, action_object_type: "WorkItem") }, class_name: "Action", foreign_key: :action_object_id
  has_one :allocation, through: :allocated_action, source: :recipient
  has_one :created_action, -> { where(action_type: 1, action_object_type: "WorkItem") }, class_name: "Action", foreign_key: :action_object_id
  has_one :creator, through: :created_action, source: :actor
  scope :enabled, -> { where(canceled_at: nil).where(finished_at: nil).order(deadline: :asc).where("deadline > ?", Time.now) }
  scope :finished, -> { where.not(finished_at: nil).order(deadline: :asc) }
  scope :canceled, -> { where.not(canceled_at: nil).order(deadline: :asc) }
  scope :overdue, -> { where(canceled_at: nil).where(finished_at: nil).where("deadline < ?", Time.now) }
  
  validates_presence_of :title, :deadline
  validate :check_allocation, on: :create
  validate :check_deadline, on: :create
  
  attr_accessor :time_amount, :time_scale, :responsible_person_id

  TIME_SCALES = [:minutes, :hours, :days, :weeks]
  TIME_AMOUNTS = (1..99).to_a

  def check_allocation
    errors.add(:responsible_person_id, "should be assigned") if responsible_person_id.empty?
  end

  def check_deadline
    min_deadline = (Time.current + 1.minute).to_i
    if deadline.present? && deadline.to_i < min_deadline
      errors.add(:deadline, "At minimum 1 minute")
    end
  end

  def set_deadline(params)
    amount = params[:time_amount].to_i
    scale = normalize(params[:time_scale])
    seconds = to_seconds(scale)
    value = amount * seconds
    self.deadline = (Time.current + value)
  end

  def set_creator(current_user)
    self.creator_id = current_user.id
  end

  def normalize(scale)
    scale.slice(-1) != "s" ? scale + "s" : scale
  end

  def to_seconds(scale)
    case scale
    when "minutes"
      60
    when "hours"
      60 * 60
    when "days"
      60 * 60 * 24
    when "weeks"
      60 * 60 * 24 * 7
    end
  end
end

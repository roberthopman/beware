Rails.application.routes.draw do      
  root "application#hello"
  require "sidekiq/web"
  require "sidekiq/cron/web"
  mount Sidekiq::Web => "/sidekiq"

  devise_for :users, controllers: {
    registrations: "registrations",
    invitations: "admin/invitations"
  }

  namespace :admin do
    resources :users do 
      post '/archive', to: "users#archive"
    end
  end

  get "account/:id", to: "application#select_tenant", as: :select_tenant
  resources :boards, path: :canvasses
  resources :cards do 
    member do 
      patch :move
    end
  end
  resources :forms, path: :questions do 
    post '/pause', to: "forms#pause"
    resources :fields do 
      resources :field_values
    end
  end
  resources :lists do 
    member do 
      get :cancel
    end
    resources :cards, module: :lists
  end
  resources :templates
  resources :work_items, except: [:show, :destroy, :edit] do 
    patch "/cancel", to: "work_items#cancel"
  end
  get '/user_account', to: 'user_accounts#show'
end

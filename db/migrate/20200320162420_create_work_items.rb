class CreateWorkItems < ActiveRecord::Migration[6.0]
  def change
    create_table :work_items do |t|
      t.string :title
      t.datetime :deadline
      t.datetime :finished_at

      t.timestamps
    end
  end
end

class AddCanceledAtToWorkItems < ActiveRecord::Migration[6.0]
  def change
    add_column :work_items, :canceled_at, :datetime
  end
end

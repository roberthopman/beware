class AddCreatorToWorkItems < ActiveRecord::Migration[6.0]
  def change
    add_reference :work_items, :creator
  end
end

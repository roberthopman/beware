class CreateNotifications < ActiveRecord::Migration[6.0]
  def change
    create_table :notifications do |t|
      t.integer :actor_id
      t.integer :recipient_id
      t.string :action
      t.string :action_object_type
      t.integer :action_object_id
      t.string :second_object_type
      t.integer :second_object_id
      t.string :job_id
      t.datetime :scheduled_at
      t.datetime :sent_at
      t.datetime :read_at

      t.timestamps
    end
  end
end

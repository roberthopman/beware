class AddAccountToModels < ActiveRecord::Migration[6.0]
  def change
    add_reference :work_items, :account, foreign_key: true
    add_reference :notifications, :account, foreign_key: true
  end
end

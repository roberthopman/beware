class AddEnumToRoles < ActiveRecord::Migration[6.0]
  def change
    remove_column :roles, :name
    add_column :roles, :role_name, :integer, unique: true
  end
end

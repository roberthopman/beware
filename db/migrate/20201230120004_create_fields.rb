class CreateFields < ActiveRecord::Migration[6.0]
  def change
    create_table :fields do |t|
      t.string :name
      t.integer :field_type
      t.integer :position
      t.references :form, null: false, foreign_key: true

      t.timestamps
    end
  end
end

class CreateFieldValues < ActiveRecord::Migration[6.0]
  def change
    create_table :field_values do |t|
      t.text :value
      t.references :form, null: false, foreign_key: true
      t.references :field, null: false, foreign_key: true

      t.timestamps
    end
  end
end

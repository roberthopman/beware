class CreateActions < ActiveRecord::Migration[6.0]
  def change
    create_table :actions do |t|
      t.integer :actor_id
      t.integer :recipient_id
      t.string :action_type
      t.string :action_object_type
      t.integer :action_object_id
      t.string :second_object_type
      t.integer :second_object_id

      t.timestamps
    end
  end
end

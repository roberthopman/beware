class CreateFormEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :form_events do |t|
      t.references :form, null: false, foreign_key: true
      t.references :event, null: false, foreign_key: true
      t.references :account, null: false, foreign_key: true

      t.timestamps
    end  
  end
end

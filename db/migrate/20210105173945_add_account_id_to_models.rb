class AddAccountIdToModels < ActiveRecord::Migration[6.0]
  def change
    add_reference :forms, :account, foreign_key: true
    add_reference :fields, :account, foreign_key: true
    add_reference :field_values, :account, foreign_key: true
    add_reference :events, :account, foreign_key: true
    add_reference :actions, :account, foreign_key: true
  end
end

class AddPausedToForms < ActiveRecord::Migration[6.0]
  def change
    add_column :forms, :paused, :boolean, default: false
  end
end

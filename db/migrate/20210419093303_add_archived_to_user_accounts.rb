class AddArchivedToUserAccounts < ActiveRecord::Migration[6.0]
  def change
    add_column :user_accounts, :archived, :boolean, default: false
  end
end

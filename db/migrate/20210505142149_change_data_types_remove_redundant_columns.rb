class ChangeDataTypesRemoveRedundantColumns < ActiveRecord::Migration[6.0]
  def change
    change_column :actions, :action_type, :integer
    
    rename_column :notifications, :action, :action_id
    remove_column :notifications, :actor_id
    remove_column :notifications, :recipient_id
    remove_column :notifications, :action_object_type
    remove_column :notifications, :action_object_id
    remove_column :notifications, :second_object_type
    remove_column :notifications, :second_object_id
  end
end

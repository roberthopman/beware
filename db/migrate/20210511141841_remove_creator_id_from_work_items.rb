class RemoveCreatorIdFromWorkItems < ActiveRecord::Migration[6.0]
  def change
    remove_column :work_items, :creator_id
  end
end

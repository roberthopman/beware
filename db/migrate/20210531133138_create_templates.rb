class CreateTemplates < ActiveRecord::Migration[6.0]
  def change
    create_table :templates do |t|
      t.string :title
      t.text :description
      t.belongs_to :account, null: false, foreign_key: true

      t.timestamps
    end
  end
end

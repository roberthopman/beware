class CreateTemplateLists < ActiveRecord::Migration[6.0]
  def change
    create_table :template_lists do |t|
      t.string :title
      t.integer :position
      t.belongs_to :template, null: false, foreign_key: true
      t.belongs_to :account, null: false, foreign_key: true

      t.timestamps
    end
  end
end

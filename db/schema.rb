# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_06_08_110533) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "actions", force: :cascade do |t|
    t.integer "actor_id"
    t.integer "recipient_id"
    t.integer "action_type"
    t.string "action_object_type"
    t.integer "action_object_id"
    t.string "second_object_type"
    t.integer "second_object_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "account_id"
    t.index ["account_id"], name: "index_actions_on_account_id"
  end

  create_table "boards", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.bigint "template_id", null: false
    t.bigint "account_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["account_id"], name: "index_boards_on_account_id"
    t.index ["template_id"], name: "index_boards_on_template_id"
  end

  create_table "cards", force: :cascade do |t|
    t.text "title"
    t.integer "position"
    t.bigint "list_id", null: false
    t.bigint "account_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["account_id"], name: "index_cards_on_account_id"
    t.index ["list_id"], name: "index_cards_on_list_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "name"
    t.text "recurring"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "account_id"
    t.index ["account_id"], name: "index_events_on_account_id"
  end

  create_table "field_values", force: :cascade do |t|
    t.text "value"
    t.bigint "form_id", null: false
    t.bigint "field_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "account_id"
    t.index ["account_id"], name: "index_field_values_on_account_id"
    t.index ["field_id"], name: "index_field_values_on_field_id"
    t.index ["form_id"], name: "index_field_values_on_form_id"
  end

  create_table "fields", force: :cascade do |t|
    t.string "name"
    t.integer "field_type"
    t.integer "position"
    t.bigint "form_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "account_id"
    t.index ["account_id"], name: "index_fields_on_account_id"
    t.index ["form_id"], name: "index_fields_on_form_id"
  end

  create_table "form_events", force: :cascade do |t|
    t.bigint "form_id", null: false
    t.bigint "event_id", null: false
    t.bigint "account_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["account_id"], name: "index_form_events_on_account_id"
    t.index ["event_id"], name: "index_form_events_on_event_id"
    t.index ["form_id"], name: "index_form_events_on_form_id"
  end

  create_table "forms", force: :cascade do |t|
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "account_id"
    t.boolean "paused", default: false
    t.index ["account_id"], name: "index_forms_on_account_id"
  end

  create_table "lists", force: :cascade do |t|
    t.string "title"
    t.integer "position"
    t.bigint "board_id", null: false
    t.bigint "account_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["account_id"], name: "index_lists_on_account_id"
    t.index ["board_id"], name: "index_lists_on_board_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.string "action_id"
    t.string "job_id"
    t.datetime "scheduled_at"
    t.datetime "sent_at"
    t.datetime "read_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "account_id"
    t.index ["account_id"], name: "index_notifications_on_account_id"
  end

  create_table "roles", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "role_name"
  end

  create_table "template_lists", force: :cascade do |t|
    t.string "title"
    t.integer "position"
    t.bigint "template_id", null: false
    t.bigint "account_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["account_id"], name: "index_template_lists_on_account_id"
    t.index ["template_id"], name: "index_template_lists_on_template_id"
  end

  create_table "templates", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.bigint "account_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["account_id"], name: "index_templates_on_account_id"
  end

  create_table "user_accounts", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "account_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "archived", default: false
    t.index ["account_id"], name: "index_user_accounts_on_account_id"
    t.index ["user_id"], name: "index_user_accounts_on_user_id"
  end

  create_table "user_roles", force: :cascade do |t|
    t.bigint "account_id", null: false
    t.bigint "user_id", null: false
    t.bigint "role_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["account_id"], name: "index_user_roles_on_account_id"
    t.index ["role_id"], name: "index_user_roles_on_role_id"
    t.index ["user_id"], name: "index_user_roles_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "first_name"
    t.string "last_name"
    t.string "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.bigint "invited_by_id"
    t.integer "invitations_count", default: 0
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["invitations_count"], name: "index_users_on_invitations_count"
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_users_on_invited_by_type_and_invited_by_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "work_items", force: :cascade do |t|
    t.string "title"
    t.datetime "deadline"
    t.datetime "finished_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "canceled_at"
    t.bigint "account_id"
    t.index ["account_id"], name: "index_work_items_on_account_id"
  end

  add_foreign_key "actions", "accounts"
  add_foreign_key "boards", "accounts"
  add_foreign_key "boards", "templates"
  add_foreign_key "cards", "accounts"
  add_foreign_key "cards", "lists"
  add_foreign_key "events", "accounts"
  add_foreign_key "field_values", "accounts"
  add_foreign_key "field_values", "fields"
  add_foreign_key "field_values", "forms"
  add_foreign_key "fields", "accounts"
  add_foreign_key "fields", "forms"
  add_foreign_key "form_events", "accounts"
  add_foreign_key "form_events", "events"
  add_foreign_key "form_events", "forms"
  add_foreign_key "forms", "accounts"
  add_foreign_key "lists", "accounts"
  add_foreign_key "lists", "boards"
  add_foreign_key "notifications", "accounts"
  add_foreign_key "template_lists", "accounts"
  add_foreign_key "template_lists", "templates"
  add_foreign_key "templates", "accounts"
  add_foreign_key "user_accounts", "accounts"
  add_foreign_key "user_accounts", "users"
  add_foreign_key "user_roles", "accounts"
  add_foreign_key "user_roles", "roles"
  add_foreign_key "user_roles", "users"
  add_foreign_key "work_items", "accounts"
end

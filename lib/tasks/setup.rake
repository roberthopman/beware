namespace :setup do
  task seed_account: :environment do 
    account_one = Account.new(name: "Foo Bar administration")
    account_one.save
    account_two = Account.new(name: "Admin Group")
    account_two.save
  end

  task seed_roles: :environment do
    Role.role_names.keys.each { |name| Role.create(role_name: name) }
  end

  task new_user_first_account: :environment do
    account = Account.first
    ActsAsTenant.current_tenant = account
    user = User.new
    user.accounts << account
    user.roles << Role.owner
    user.update(
      name: "Foo Bar",
      email: "0@0.com",
      password: "123123",
      password_confirmation: "123123"
    )
  end

  task another_user_first_account: :environment do
    account = Account.first
    ActsAsTenant.current_tenant = account
    user = User.new
    user.accounts << account
    user.roles << Role.admin
    user.update(
      name: "Baz XYZ",
      email: "001@0.com",
      password: "123123",
      password_confirmation: "123123"
    )
  end

  task add_second_account_to_first_user: :environment do
    user = User.first
    account_two = Account.second
    ActsAsTenant.current_tenant = account_two
    user.accounts << account_two
    user.roles << Role.owner
  end

  task data: [
    :seed_account, 
    :seed_roles, 
    :new_user_first_account, 
    :add_second_account_to_first_user, 
    :another_user_first_account
  ]
end

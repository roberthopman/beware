require "test_helper"
require "rake"

class UserInteractionTest < ActionDispatch::IntegrationTest
  setup do
    WorkItem.destroy_all
    @bs = Faker::Company.bs
    load File.expand_path("../../../lib/tasks/setup.rake", __FILE__)
    Rake::Task.define_task(:environment)
    Rake::Task["setup:data"].invoke
  end

  describe "user in browser" do
    it "creates and finishes workitem" do
      visit("/")
      login = all(".nav-link").last
      login.click
      visit work_items_path
      click_on("New work item")
      fill_in("work_item_title", with: @bs)
      click_on("submit")
      assert(page.has_content?(@bs))
      click_on("V")
      assert_not(page.has_content?(@bs))
    end

    it "creates and cancels workitem" do
      visit("/")
      login = all(".nav-link").last
      login.click
      visit work_items_path
      click_on("New work item")
      fill_in("work_item_title", with: @bs)
      click_on("submit")
      assert(page.has_content?(@bs))
      click_on("X")
      assert_not(page.has_content?(@bs))
    end

    it "looks at finished workitem" do
      visit("/")
      login = all(".nav-link").last
      login.click
      visit work_items_path
      click_on("New work item")
      fill_in("work_item_title", with: @bs)
      click_on("submit")
      assert(page.has_content?(@bs))
      click_on("V")
      click_on("Finished")
      assert(page.has_content?(@bs))
    end

    it "sets amount of time to deadline" do
      random_amount = (1..99).to_a.sample
      random_scale =
        if random_amount > 1
          [:minutes, :hours, :days, :weeks].sample.to_s
        else
          [:minute, :hour, :day, :week].sample.to_s
        end
      visit("/")
      login = all(".nav-link").last
      login.click
      visit work_items_path
      click_on("New work item")
      fill_in("work_item_title", with: @bs)

      select random_amount, from: "work_item_time_amount"
      select random_scale, from: "work_item_time_scale"
      assert(page.find("select#work_item_time_amount").has_content?(random_amount))
      assert(page.find("select#work_item_time_scale").has_content?(random_scale))
      click_on("submit")
      assert(page.has_content?(@bs))
    end
  end
end

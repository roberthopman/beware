require "test_helper"
require "rake"

class UserInteractionTest < ActionDispatch::IntegrationTest
  setup do
    @name = Faker::Name.name
    @email = Faker::Internet.email
    load File.expand_path("../../../lib/tasks/setup.rake", __FILE__)
    Rake::Task.define_task(:environment)
    Rake::Task["setup:data"].invoke
  end

  describe "user inviting" do
    it "does not create user in account without role" do
      visit("/")
      login = all(".nav-link").last
      login.click
      click_link("navbarDropdownMenuLink")
      visit admin_users_path
      user_account_one = UserAccount.last
      click_button("Invite user")
      fill_in("user_name", with: @name)
      fill_in("user_email", with: @email)
      find(:xpath, '//*[@id="new_user"]/div[4]/input').click
      user_account_two = UserAccount.last
      assert(user_account_one == user_account_two)
    end

    it "does create user in account with role" do
      visit("/")
      login = all(".nav-link").last
      login.click
      click_link("navbarDropdownMenuLink")
      visit admin_users_path
      user_account_one = UserAccount.last
      click_button("Invite user")
      find(:xpath, '//*[@id="new_user"]/div[3]/div/div').click
      find(:xpath, '//*[@id="choices--dropdown-choice-select-item-choice-1"]').click
      fill_in("user_name", with: @name)
      fill_in("user_email", with: @email)
      find(:xpath, '//*[@id="new_user"]/div[4]/input').click
      user_account_two = UserAccount.last
      assert(user_account_one != user_account_two)
    end
  end
end

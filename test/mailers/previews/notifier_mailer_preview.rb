# Preview all emails at http://localhost:3000/rails/mailers/notifier_mailer
class NotifierMailerPreview < ActionMailer::Preview
  def notify
    current_user = User.first
    w = WorkItem.last
    unless w
      w = WorkItem.new(
        title: "foobar",
        deadline: DateTime.now + 5.minutes
      )
      w.set_creator(current_user)
      w.save
      Notification.schedule_notifications(w)
    end
    notification = Notification.last
    minutes = (notification.work_item.deadline - notification.scheduled_at) / 60
    rounded_min = minutes.ceil
    payload = "#{rounded_min} minutes to: " + notification.work_item.title
    NotifierMailer.notify(notification, payload)
  end
end

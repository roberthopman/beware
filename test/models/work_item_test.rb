require "test_helper"

class WorkItemTest < ActiveSupport::TestCase
  test "should not save without parameters" do
    work_item = WorkItem.new
    assert_not work_item.save
  end

  test "should not save without deadline parameters" do
    work_item = WorkItem.new(title: Faker::Company.bs)
    assert_not work_item.save
  end

  test "should not save below 1 minute deadline" do
    work_item = WorkItem.new(title: Faker::Company.bs, deadline: Time.now + 59.seconds)
    assert_not work_item.save
  end

  test "should save with title and 1 minute deadline parameters" do
    work_item = WorkItem.new(title: Faker::Company.bs, deadline: Time.now + 1.minute)
    assert_not work_item.save
  end
end
